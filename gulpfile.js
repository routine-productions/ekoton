"use strict";

//  --------------------------------------------------------------------------------------------------------------------
// Require packages
//  --------------------------------------------------------------------------------------------------------------------

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    minifyCss = require('gulp-clean-css'),
    uglify = require('gulp-uglify'),
    autoprefixer = require('gulp-autoprefixer'),
    rename = require("gulp-rename"),
    concat = require("gulp-concat"),
    combineMq = require('gulp-combine-mq'),
    svgSprite = require('gulp-svg-sprite');

//  --------------------------------------------------------------------------------------------------------------------


//  --------------------------------------------------------------------------------------------------------------------
// Watch changes in files
//  --------------------------------------------------------------------------------------------------------------------

gulp.task('watch', function () {
    gulp.watch('./scss/**/*.scss', ['scss']);
    gulp.watch('./js/**/*.js', ['js']);
    gulp.watch('./img/src/*.svg', ['svg']);
});

//  --------------------------------------------------------------------------------------------------------------------



//  --------------------------------------------------------------------------------------------------------------------
// scss
//  --------------------------------------------------------------------------------------------------------------------

gulp.task('scss', function () {
    gulp.src('./scss/init.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(combineMq({
            beautify: false
        }))
        .pipe(autoprefixer({browsers: ['> 1%', 'last 2 version', 'IE 9-11'], cascade: false}))
        .pipe(minifyCss())
        .pipe(rename('index.min.css'))
        .pipe(gulp.dest('./wp-content/themes/ecoton/'));
});

//  --------------------------------------------------------------------------------------------------------------------



//  --------------------------------------------------------------------------------------------------------------------
// js
//  --------------------------------------------------------------------------------------------------------------------

gulp.task('js', function ()
{

    gulp.src('./js/**/*.js')
        .pipe(concat('z-routine.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./wp-includes/js'));
});

//  --------------------------------------------------------------------------------------------------------------------



//  --------------------------------------------------------------------------------------------------------------------
gulp.task('svg', function()
{
    var baseDir      = './img/src/',
        svgGlob      = '**/*.svg',
        outDir       = './img/';

    var svgSpriteConfig = {
        "dest": "./img/src/",
        "log": "verbose",
        "svg": {
            "xmlDeclaration": false,
            "dimensionAttributes": false
        },
        "mode": {
            "symbol": {
                "dest": ".",
                "sprite": "sprite.svg"
            }
        }
    };
    
    return gulp.src(svgGlob, {cwd: baseDir})
        .pipe(svgSprite(svgSpriteConfig))
        .pipe(gulp.dest(outDir))
});

//  --------------------------------------------------------------------------------------------------------------------