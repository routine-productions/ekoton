<?php
/*
Template Name: About
*/
get_header();
$aboutImg = get_field('about_image');
$aboutHead = get_field('about_head');
?>


<div class="Ahead-Wrap">
    <div <?php if (isset($aboutImg['url'])) { ?>
        style="background: url('<?= $aboutImg['url'] ?>') no-repeat center / cover"
    <?php } else {

        $noImage = 'No-Image';
    }; ?> class="Ahead <?php if (isset($noImage)) {
        echo $noImage;
    }; ?>">
<!--        <div class="filter" style="position: absolute; top:0;right:0;bottom:0; opacity: .65; left:0;background:url('/img/pic/filter.png')"></div>-->
        <div class="Ahead-Content">
            <div class="Ahead-Description ">
                <?php if (isset($aboutHead)) { ?>
                    <span><?= $aboutHead ?></span>
                <?php }; ?>

            </div>
        </div>

    </div>


</div>

<div id="site-content" class="site-content Page-About">
    <section class="About">
        <h2 class="Static-Title"><span><?= the_title() ?></span></h2>
        <div class="About-Content">

            <div class="static">

                <?php
                echo the_content(the_post()); ?>
            </div>
            <?php
            get_template_part('template-parts/slider');
            ?>

        </div>
    </section>


</div>
<?php


get_footer();
?>
