<?php
/*
Template Name: Catalog
*/;

?>
<?php
get_header();
?>
<?php

function get_parent_categories_id($id, $link = false, $separator = '/', $nicename = false, $visited = array())
{
    $categories = [];
    $category = get_term($id, 'category');
    $categories[] = $category;

    while ($category->parent) {
        $categories[] = get_term($category->parent, 'category');
        $category = get_term($category->parent, 'category');
    }

    $category_id = $categories[count($categories) - 1]->term_id;

    return $category_id;
}


$thisCatObj = get_category(get_query_var('cat'), false);
$thisCat = $thisCatObj->term_id;
$parentCat = get_parent_categories($thisCat);


$cats_tree = get_cat_tree(get_parent_categories_id($thisCat), get_categories());
function get_cat_tree($parent, $categories)
{
    $result = array();
    foreach ($categories as $category) {
        if ($parent == $category->category_parent) {
            $category->children = get_cat_tree($category->cat_ID, $categories);
            $result[] = $category;
        }
    }
    return $result;
}

function cats($cats_tree){

echo '<div class="Cat-Wrap"><ul class="Cat">';
foreach ($cats_tree as $key => $cat) { ?>

<li class="Cat-In">
    <span class="Cat-Container">
        <a href="/category/catalog/<?= $cat->slug ?>" class="Cat-Link"><?= $cat->name ?></a></span>
    <?php
    if (!empty($cat->children)) {
        echo '<button class="cat-show"></button>';
        cats($cat->children);
    };
    echo '</li>';
    ?>
    <?php }
    echo '</ul></div>';

    }
    ?>
<?php
    function compareId($thisCat, $parentCat)
    {
        if (isset($parentCat)) {
            return $parentCat;
        } else {
            return $thisCat;
        }
    }

    $currentCat = compareId($thisCat, $parentCat);

    $args = [
        'cat' => $cat,
        'paged' => $paged,
        'meta_key' => 'price',
        'orderby' => 'meta_value_num'
    ];


    $curQuery = '<li data-attr="?order=price">цена по убыванию</li>';
    if (isset($_GET['order'])) {
        if ($_GET['order'] == 'price') {

            $args['order'] = 'DESC';
            $args['meta_key'] = 'price';
            $args['orderby'] = 'meta_value_num';
            $curQuery = '<li data-attr="?order=price">цена по убыванию</li>';
        }
        if ($_GET['order'] == 'price_asc') {
            $args['order'] = 'ASC';
            $args['meta_key'] = 'price';
            $args['orderby'] = 'meta_value_num';
            $curQuery = '<li data-attr="?order=price_asc">цена по возрастанию</li>';

        }
        if ($_GET['order'] == 'views') {
            $args['order'] = 'DESC';
            $args['meta_key'] = 'views';
            $args['orderby'] = 'meta_value_num';
            $curQuery = '<li data-attr="?order=views">по популярности</li>';


        }
        if ($_GET['order'] == 'date') {
            $args['order'] = 'DESC';
            $args['orderby'] = 'meta_value_num';
            $curQuery = '<li data-attr="?order=date">по новизне</li>';
        }
    }


    ?>

    <div class="Catalog-Wrap Brands-Catalog">
        <header class="Catalog-Header">
            <h2 class="Catalog-Title"><span>
                <?php echo get_cat_name($currentCat); ?>
                </span></h2>
            <div class="Cat-Menu Js-Close-Cat">
                <svg>
                    <use xlink:href="#gear"></use>
                </svg>
            </div>

            <div class="Sorting-Block">
                <span class="Sorting-Title">Сортировка:</span>

                <div class="Sorting-Wrap">
                    <div class="Sorting">
                        <div class="Current-Condition">
                            <ul class="Js-Condition">
                                <?= $curQuery ?>
                            </ul>
                            <div class="Arrow">
                                <svg>
                                    <use xlink:href="#down"></use>
                                </svg>
                            </div>
                        </div>
                        <div class="Conditions-Wrap">

                            <ul class="Conditions">
                                <li data-href="?order=views"><a href="?order=views">по популярности</a></li>
                                <li data-href="?order=price"><a href="?order=price">цена по убыванию</a></li>
                                <li data-href="?order=price_asc"><a href="?order=price_asc">цена по возрастанию </a>
                                </li>
                                <li data-href="?order=date"><a href="?order=date">по новизне</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>

        </header>
        <div class="Catalog-Content">
            <aside class="Catalog-Aside-Wrap Js-Category">
                <nav class="Catalog-Aside">
                    <h3 class="Aside-Title"><span>Категории товаров</span>
                        <button class="Js-Close-Cat">
                            <svg>
                                <use xlink:href="#close"></use>
                            </svg>
                        </button>
                    </h3>
                    <div class="Catalog-Categories All-Categories">
                        <?php
                        cats($cats_tree);
                        ?>
                    </div>

                </nav>

            </aside>

            <div class="Catalog-Posts">
                <?php

                query_posts($args);

                while (have_posts()) {
                    the_post();
                    get_template_part('template-parts/post-catalog');
                }

                ?>
                <div class="Paginate">
                    <?= paginate_links(); ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    get_footer();
    ?>
