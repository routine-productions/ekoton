<div class="Post-News-Wrap">
    <div class="Post-News">
        <div class="Image">
            <div style="padding-bottom: 77%;background:  url('<?= get_field('post-image')['url']; ?>') no-repeat center/ contain"></div>
        </div>
        <article class="News-Article">
            <h2><span><?php the_title(); ?></span></h2>

            <div class="Short-Description static">
                <p>
                    <?php
                    trim_content_field(strip_tags(get_post_meta(get_the_id(), 'short-description', true)), 120, '...');
                    ?>
                </p>
            </div>
            <div class="News-Info">
                <span class="News-Date">
                    <?= get_the_date(); ?>
                </span>

                <a href="<?= the_permalink() ?>">Подробнее</a>
            </div>
            <?php
            ?>
        </article>
    </div>
</div>





