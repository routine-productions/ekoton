<?php the_post();
$postId = get_the_id();
?>
<div class="One-News-Page">
    <article class="One-News-Wrap Single-Post">
        <h2 class="News-Title">
            <div><span class="Date"><?= get_the_date(); ?></span>

                <span class="Title"><?php the_title(); ?></span>
                <div class="Breadcrumbs">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/category/news/">Новости</a></li>
                </div>
            </div>
        </h2>

            <?php
            $postImage = get_field('post-image')['url'];
            if (!empty($postImage)) {
                ?>
        <div class="Image">
                <div style="padding-bottom: 77%; background: url('<?= $postImage; ?>')no-repeat center / contain "></div>
        </div>
            <?php };
            ?>



        <div class="One-News">
<!--            <div class="Short-Description static">-->
<!--                --><?php
//                echo get_post_meta(get_the_id(), 'short-description', true);
//                ?>
<!---->
<!--            </div>-->
            <div class="News-Content static">
                <?php the_content() ?>
            </div>

        </div>
    </article>


</div>
<div class="Latest-News-Wrap">
    <section class="Latest-News">

        <h2><span>Последние новости</span></h2>
        <?php

        $latest_news = get_posts([
            'category' => get_the_category()[0]->term_id,
            'posts_per_page' => 4,
            'exclude' => $postId
        ]);

        foreach ($latest_news as $latest_one_news) {
            $post = $latest_one_news;
            get_template_part('template-parts/post-news-small');

        }
        ?>

    </section>
</div>
