<?php
$slides = get_field('gallery');
$postImg = get_field('post-image')['url'];

if (isset($postImg) || !empty($slides)) { ?>
    <div class="Slider Js-Slider">
        <div class="Slider-Box">
            <div class="View">
                <?php
                if (!empty($slides)) {
                    ?>
                    <div class="View-Item"
                         style="background: url('<?= $slides[0]['image']['url'] ?>')no-repeat center / contain"></div>

                <?php } else {
                    ?>
                    <div class="View-Item" style="background-image: url('<?= $postImg ?>')"></div>
                <?php }
                ?>
            </div>
            <?php
            if (!empty($slides)) {
                ?>
                <div class="Views">
                    <div class="Views-List">
                        <?php
                        if (count($slides) > 1) {

                            foreach ($slides as $slide) { ?>

                                <div class="Js-Img" data-attr="<?= $slide['image']['url'] ?>"
                                     style="height: 75px;width: 75px; display: inline-block;background-image: url('<?= $slide['image']['url'] ?>')"></div>

                            <?php }
                        }
                        ?>
                    </div>
                </div>
            <?php }
            ?>
        </div>
    </div>
<?php } ?>