<?php
$postId = get_the_id();
$thisCatObj = get_category(get_query_var('cat'), false);
$thisCat = get_the_category($postId)[0];
$parCat = get_category($thisCat->parent);
$granCat = get_category($parCat->category_parent);
?>
<div class="Post-Wrap">
    <section class="Post Js-Post">
        <h3 class="Post-Title"><a class="Js-Link" href="<?= the_permalink() ?>">
                <div class="Post-Image">
                    <div class="centrator">
                        <div class="Cell">
                            <?php if (!empty(get_field('post-image')['url'])) { ?>
                                <img src="<?= get_field('post-image')['url']; ?>" alt="">
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <span class="Message-Title Js-Title"><?php the_title(); ?></span></a></h3>

        <div class="Post-Category Brands-Category">
            <?php
            echo get_category_parents($thisCat, true, '');

            ?>
        </div>
        <div class="Post-Content">
            <div class="Short-Description static">
                <p>
                    <?php
                    trim_content_field(strip_tags(get_post_meta($postId, 'short-description', true)), 120, '...');
                    ?>
                </p>
            </div>
            <div class="Post-Footer">
                <?php
                $price = get_post_meta($postId, 'price', true);
                if (!empty($price)) {
                    echo '<div class="Price"><span class="price">' . number_format($price, 0, '', ' ') . '</span><svg class="Rub"><use xlink:href="#rub"></use></svg></div>';
                }
                echo '<div class="Order-Wrap"><button class="Order Js-Order">Заказать</button></div>';
                ?>
            </div>
        </div>
    </section>
</div>




