<?php

the_post();
$postId = get_the_id();
$thisCat = get_the_category($postId)[0];
$parCat = get_category($thisCat->parent);
$granCat = get_category($parCat->category_parent);


function parent_category_list($thisCat, $cats)
{
    if (! empty($thisCat) && $thisCat->parent > 0)
    {
        $thisCat = get_category($thisCat->parent);
        $cats[] = $thisCat;
        $cats = parent_category_list($thisCat, $cats);
    }

    return $cats;
}

$categories[] = $thisCat;
$categories = array_reverse(parent_category_list($thisCat, $categories));

//print_r($categories);
?>
<section class="Post-Extended-Page Js-Post">
    <header>
        <h2><span class="Js-Title"><?= get_the_title($postId); ?></span>
            <a class="Js-Link Hidden" href="<?= the_permalink() ?>"></a>
            <ul class="Breadcrumbs">
                <li><a href="/"><span>Главная</span></a></li>
                <?php

                foreach ($categories as $value)
                {
                    echo ' <li><a href="/category/catalog/' . $value->slug . '"><span>' . $value->name . '</span></a></li>';
                }
                ?>

            </ul>
        </h2>

    </header>

    <?php
    get_template_part('template-parts/slider');
    ?>
    <div class="Post-Extended-Wrap">
        <div class="Post-Extended">
            <div class="Full-Content static">
                <?php
                echo the_content();
                ?>
            </div>
            <div class="Post-Footer">
                <?php
                $price = get_post_meta($postId, 'price', true);
                if (! empty($price))
                {
                    echo '<div class="Price"><span class="Price-Name">Цена:</span><span class="price">' . number_format($price, 0, '', ' ') . '</span><svg class="Rub"><use xlink:href="#rub"></use></svg></div>';
                }
                echo '<div class="Order-Wrap"><button class="Order Js-Order">Заказать</button></div>';
                ?>
            </div>
            <div class="Post-Tabs JS-Tabs">
                <nav class="Tabs-Nav JS-Tabs-Navigation">
                    <?php if (get_field('description'))
                    { ?>
                        <a data-href="Tab-1" class="JS-Tab Active">Описание</a>
                    <?php }
                    if (get_field('characteristics'))
                    {
                        ?>
                        <a data-href="Tab-2" class="JS-Tab">Характеристики</a>
                    <?php }
                    if (get_field('operating-principle'))
                    {
                        ?>
                        <a data-href="Tab-3" class="JS-Tab">Принцип действия</a>
                    <?php } ?>
                </nav>
                <ul class="Tabs-Content JS-Tabs-Content">

                    <li data-tab='Tab-1' class="Tab static">
                        <span class="Tab-Head">Описание</span>
                        <?php echo get_field('description'); ?>
                    </li>
                    <li data-tab='Tab-2' class="Tab static">
                        <span class="Tab-Head">Характеристики</span>
                        <?php echo get_field('characteristics'); ?>
                    </li>
                    <li data-tab='Tab-3' class="Tab Active static">
                        <span class="Tab-Head">Принцип действия</span>
                        <?php echo get_field('operating-principle'); ?>
                    </li>
                </ul>

            </div>


        </div>
    </div>
</section>
<?php

$latest_posts = get_posts([
    'category' => get_the_category()[0]->term_id,
    'posts_per_page' => 4,
    'exclude' => $postId
]);

if (! empty($latest_posts))
{
    ?>

    <div class="Similar-Posts-Wrap">
        <section class="Similar-Posts">

            <h2><span>Похожие товары</span></h2>
            <?php


            foreach ($latest_posts as $latest_post)
            {
                $post = $latest_post;
                get_template_part('template-parts/post');

            }
            ?>

        </section>
    </div>

<?php }
?>





