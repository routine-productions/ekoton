<div class="Sorting-Block">
    <span class="Sorting-Title">Сортировка:</span>
    <div class="Sorting-Wrap">
        <div class="Sorting">
            <div class="Current-Condition">
                <ul class="Js-Condition">
                    <li><a href="">по умолчанию</a></li>
                </ul>
                <div class="Arrow">
                    <svg>
                        <use xlink:href="#down"></use>
                    </svg>
                </div>
            </div>
            <div class="Conditions-Wrap">
                <ul class="Conditions">
                    <li><a href="?=views">по популярности</a></li>
                    <li><a href="?=price">цена по убыванию</a></li>
                    <li><a href="?=price_asc">цена по возрастанию </a></li>
                    <li><a href="?=date">по новизне</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>