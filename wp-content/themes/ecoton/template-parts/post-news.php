<div class="Post-News-Wrap">
    <div class="Post-News">
        <div class="Image">
            <div style="padding-bottom: 77%;background:  url('<?= get_field('post-image')['url']; ?>') no-repeat center/ contain"></div>
        </div>
        <article class="News-Article">
            <h2><span><?php the_title(); ?></span></h2>

            <div class="Short-Description static">
                <p>
                    <?php
                    trim_content_field(get_field('short-description'),120,'...');
                    ?>
                </p>
            </div>
            <div class="News-Info">
                <span class="News-Date">
                    <?= get_the_date(); ?>
                </span>

                <a href="<?= the_permalink() ?>">Подробнее</a>
            </div>
            <?php
            ?>
        </article>
    </div>
</div>





