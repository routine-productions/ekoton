
<form role="search" method="get" class="Search-Form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<input type="search" class="Search-Field" placeholder="<?php echo esc_attr_x( 'Поиск по каталогу &hellip;', 'placeholder', 'twentysixteen' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
	</label>
	<button type="submit" class="Search-Submit">
	<svg><use xlink:href="#search"></use></svg>
	</button>
</form>
