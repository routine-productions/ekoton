<?php
/*
Template Name: Контакты
*/

get_header();

?>
    <div id="site-content" class="site-content Contact">

        <div class="static">
            <h2 class="">Наши контакты</h2>
            <?= the_content(the_post()); ?>
        </div>
        <div class="Contact-Form">
            <h2 class="Form-Title"><span>Связаться с нами</span></h2>
            <form class="Order-Content Form JS-Form"
                  data-form-email='ewgen.p@yandex.ua'
                  data-form-url='/data.form.php'
                  data-form-method='POST'>
                <input data-input-title='Имя заказчика' class="JS-Form-Require" type="text" placeholder="Ваше имя"
                       name="name">
                <input data-input-title='Телефон' class="JS-Form-Numeric JS-Form-Require" type="number" placeholder="Телефон"
                       name="phone">
                <input data-input-title='e-mail' class="JS-Form-Email" type="text"
                       placeholder="Email" name="email">
                <textarea data-input-title="Сообщение заказчика" placeholder="Сообщение"></textarea>
                <input class="JS-Form-Button" type="submit" value="Отправить">
                <span class="JS-Form-Result"></span>
            </form>
        </div>
    </div>

    <div class="Contact-Map">
        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=ZIGiMOa0QaPqpfCcf8vQKWRVeZjswQgd&amp;width=100%&amp;height=300&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true"></script>
    </div>
<?php


get_footer();
?>