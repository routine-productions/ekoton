<?php
get_header();
?>
<section class="Popular-Offers">
    <h2 class="Special-Title"><span>Популярное оборудование</span></h2>
    <div class="More">
        <a href="/category/catalog/">Перейти в каталог</a>
    </div>
    <div class="Popular-Posts">
        <?php
        $args = [
            'cat' => 4,
            'posts_per_page' => 6,
            'meta_key' => 'views',
            'orderby' => 'meta_value_num'
        ];
        query_posts($args);

        while (have_posts()) {
            the_post();
            get_template_part('template-parts/post-catalog');
        }
        wp_reset_query();

        ?>
    </div>
</section>
<div class="Go-Catalog">
    <a href="/category/catalog/">Перейти к каталогу</a>
</div>
<div class="Home-Content">
    <?php
    get_post();

    ?>
    <?php

    $categories = get_categories(['child_of' => 4,
        'parent' => 4]);
    $thisCatObj = get_category(get_query_var('cat'), false);
    $thisCat = $thisCatObj->term_id;
    $parentCat = get_parent_categories($thisCat);

    ?>
    <div class="Advantages-Wrap fixed-bg">
        <div class="Advantages">
            <ul class="List">
                <li><?php echo get_field('experience'); ?></li>
                <li><?php echo get_field('succesful'); ?></li>
                <li><?php echo get_field('clients'); ?></li>
            </ul>
        </div>
    </div>
    <div class="Providers-Wrap Wrap">
        <section class="Providers">
            <h2><span>Наши поставщики</span></h2>
            <ul class="Providers-List">
                <?php
                $count = 0;
                foreach ($categories as $category) {
                    $catId = $category->cat_ID;
                    $catParentId = $category->category_parent;
                    $catName = $category->cat_name;
                    $catSlug = $category->slug;
                    $catDescription = category_description($catId);
                    $catAdd = get_all_wp_terms_meta($catId);
                    if ($category) { ?>
                        <li>
                            <div class="Img-Handler"><img src="<?= $catAdd['brand-logo'][0] ?>" alt="<?= $catName ?>"/>
                            </div>
                            <div class="Provider"><?= trim_content_field($catDescription, 200, '...'); ?></div>
                            <div class="Provider-Footer">
                                <a href="/category/catalog/<?= $catSlug ?>"><?= 'Перейти к продукции ' . $catName ?></a>
                            </div>

                        </li>
                        <li class="t-caption"></li>
                    <?php }
                    $count++;
                }
                ?>
            </ul>

        </section>
    </div>
    <div class="Customer-Wrap Wrap fixed-bg">
        <section class="Customer">
            <h2><span>Наши услуги</span></h2>
            <ul class="List">
                <li>
                    <a class="Customer_link"  href="/montage">
                        <div class="Svg-Holder maintenance">
                            <svg>
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#maintenance"></use>
                            </svg>
                        </div>
                        <span>Шеф-монтаж</span>
                        <span class="Customer_description">Наши специалисты выезжают на предприятие, чтобы смонтировать и запустить оборудование</span>
                    </a>
                </li>
                <li>
                    <a class="Customer_link" href="/engineering">
                        <div class="Svg-Holder a-cad">
                            <svg>
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#a-cad"></use>
                            </svg>
                        </div>
                        <span>Инжиниринг</span>
                        <span class="Customer_description">Все наши решения принимаются на основе оптимального технического решения</span>
                    </a>
                </li>
                <li>
                    <a class="Customer_link" href="/maintenance">
                    <div class="Svg-Holder board">
                        <svg>
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#board"></use>
                        </svg>
                    </div>
                    <span>Сервисная поддержка</span>
                    <span class="Customer_description">Мы отвечаем за эффективность работы оборудования на протяжении всего периода эксплуатации</span>
                    </a>
                </li>
            </ul>
        </section>
    </div>
    <div class="About-Us-Wrap Wrap">
        <section class="About-Us">
            <h2><span>О Компании</span></h2>
            <div class="More"><a href="/about">Подробнее</a></div>
            <div class="Double-Time static">
                <p>ООО «ЭКОТОН-РУС» является поставщиком инженерного оборудования для объектов атомной и тепловой
                    энергетики, нефтяной, газоперерабатывающей, горнодобывающей и химической промышленности,
                    металлургии, объектов жилищно-коммунального хозяйства и строительства.</p>
                <p>ООО «ЭКОТОН-РУС» является официальным дистрибьютором заводов-производителей, таких как НПФ «ЭКОТОН»,
                    ФГУП «СПО «АНАЛИТПРИБОР», FLYGT, LOWARA, VOGEL, WEDECO, ZPA-PECKY и многих других, а также
                    осуществляет инженерно-техническую поддержку, ремонтные, шеф-монтажные и сервисные работы
                    поставляемого оборудования.</p>
                <p>Оборудование, поставляемое нашей компанией, применяется в технологических процессах, на очистных
                    сооружениях, для водоснабжения, водоотведения, перекачки абразивных, шламовых жидкостей, для
                    хранения жидких и радиоактивных отходов.</p>
                <p>В перечень поставляемой продукции входят: насосы и насосные станции, трубопроводная запорная
                    арматура, газоанализаторы, щиты управления, электроприводы, ультрафиолетовые системы и генераторы
                    озона, аэрационные системы, эжекторы, мешалки, оборудование для отстойников и механического
                    обезвоживания осадка, песколовки, фильтр-прессы, механизированные решетки, блочные очистные
                    сооружения, комплексы водоподготовки. </p>
                <p>Основным направлением деятельности Промышленной Группы ЭКОТОН является разработка, производство и
                    внедрение современных высокоэффективных технологий и оборудования для очистки сточных вод на
                    объектах водоснабжения и водоотведения муниципальных организаций и промышленных предприятий.
                    В своей деятельности Промышленная группа ЭКОТОН ориентируется на применение новейших достижений
                    науки и техники, большое внимание уделяется исследованиям в области водоснабжения и водоотведения
                    для различных отраслей промышленности. </p>
            </div>
        </section>
    </div>
    <div class="Our-Projects-Wrap Wrap">
        <section class="Our-Projects">
            <h2><span>Наши проекты</span></h2>
            <ul class="List">
                <li>
                    <div class="One-Project static">
                        <?php trim_content_field(get_field('project1'), 305, '...'); ?>
                    </div>
                </li>
                <li>
                    <div class="One-Project static">
                        <?php trim_content_field(get_field('project2'), 305, '...'); ?>
                    </div>
                </li>
                <li>
                    <div class="One-Project static">
                        <?php trim_content_field(get_field('project3'), 305, '...'); ?>
                    </div>
                </li>
            </ul>
        </section>
    </div>

    <div class="Home-News-Wrap">
        <section class="Home-News">
            <h2><span>Новости</span></h2>
            <div class="More">
                <a href="/category/news/">К новостям</a>
            </div>
            <div class="News-List">
                <?php

                $newest_news = get_posts([
                    'category' => 11,
                    'posts_per_page' => 2
                ]);

                foreach ($newest_news as $latest_new) {
                    $post = $latest_new;
                    get_template_part('template-parts/post-news-small');
                }


                ?>
            </div>
        </section>
    </div>

</div>

<?php get_footer(); ?>
