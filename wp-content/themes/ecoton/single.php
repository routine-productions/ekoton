<?php

get_header(); ?>
<main id="site-content" class="Site-Content Single-Post">
<?php

if (in_category(11)) {
    include(TEMPLATEPATH.'/single-news.php');
} else {
    include(TEMPLATEPATH.'/single-default.php');
};
?>
</main>
<?php get_footer();
?>

