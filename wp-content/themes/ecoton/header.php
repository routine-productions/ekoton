<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=PT+Serif:400,400i,700,700i&subset=cyrillic-ext"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="stylesheet" href="/wp-content/themes/ecoton/index.min.css">

</head>

<body <?php body_class(); ?>>
<!--SVG-SPRITE-->
<?php
require __DIR__ . '/../../../img/sprite.svg';
?>
<!--Site-HEADER-->
<header id="master-head" class="Site-Header-Wrap" role="banner">
    <div class="Site-Header-Top">
        <div class="Header-Top">
            <div class="Site-Branding">
                <?php if (is_front_page() && is_home()) : ?>
                    <h1 class="Site-Title"><a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                            <svg>
                                <use xlink:href="#ekoton"></use>
                            </svg>
                            <span><?php bloginfo('name'); ?></span></a></h1>
                <?php else : ?>
                    <h1 class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                            <svg>
                                <use xlink:href="#ekoton"></use>
                            </svg>

                            <span><?php bloginfo('name'); ?></span></a></h1>

                <?php endif; ?>
                <div class="Ham-Wrap">
                    <span class="Ham"></span>
                </div>
            </div>
            <nav class="Site-Nav">
                <div class="Nav">
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'primary',
                        'menu_class' => 'primary-menu',
                    ));

                    ?>
                    <div class="Header-Phone">
                        <div class="Phone">
                            <?php
                            dynamic_sidebar('sidebar-3');
                            ?>
                            <button class="JS-Button"><span>Заказать звонок!</span></button>
                        </div>
                    </div>
                </div>
            </nav>

        </div>
    </div>
    <div class="Site-Header-Bottom">

        <?php
        $categories = get_categories(['child_of' => 4,
            'parent' => 4,
            'hide_empty' => 0,
            ]);
        $thisCatObj = get_category(get_query_var('cat'), false);
        $thisCat = $thisCatObj->term_id;
        $parentCat = get_parent_categories($thisCat);

        ?>
        <div class="Header-Bottom">
            <ul class="Provider-Categories">
                <?php

                foreach ($categories as $key => $category)
                {
                    $catId = $category->cat_ID;
                    $catParentId = $category->category_parent;
                    $catName = $category->cat_name;
                    $catLink = get_category_link($catId);
                    $catAdd = get_all_wp_terms_meta($catId);
//                    if ($key < 9)
//                    {
                        echo '<li><a href="' . $catLink . '">' . $catName . '</a></li>';
//                    }

                }
                ?>
            </ul>
            <div class="Catalog-Search">
                <?php
                echo get_search_form();
                ?>
            </div>
        </div>
    </div>
</header>
<?php
if ($_SERVER['REQUEST_URI'] == '/')
{

    echo '<div class="Main-Slider">' . do_shortcode("[metaslider id=4]") . '</div>';

} else
{
    return false;
};
?>
<!--SITE-CONTENT-->
<main id="site-content" class="site-content">
