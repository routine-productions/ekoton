<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

</main><!-- .site-content -->

<footer id="site-footer" class="Site-Footer" role="contentinfo">
    <nav class="Footer-Nav">
        <div class="Footer-Menus">
            <?php
            dynamic_sidebar('sidebar-2');
            ?>
        </div>
    </nav>
    <div class="Copyright-Wrap">
        <div class="Copyright">
            <?php
            $description = get_bloginfo('description', 'display');
            if ($description || is_customize_preview()) : ?>
                <span class="site-description"><?php echo $description; ?></span>
            <?php endif; ?>

            <div class="Routine">
                <a target="_blank" href="http://routine.productions/">
                    <svg>
                        <defs>
                            <radialGradient id="Sign_Monochrome_1_" cx="252.05" cy="140.05" r="103.6005"
                                            gradientTransform="matrix(1 0 0 -1 0 246)" gradientUnits="userSpaceOnUse">
                                <stop offset="0" style="stop-color:#FFA000"/>
                                <stop offset="1" style="stop-color:#FF6F00"/>
                            </radialGradient>
                            </radialGradient>
                        </defs>
                        <use xlink:href="#routine"></use>
                    </svg>
                </a>
            </div>
        </div>
    </div>
    <div class="Form-Wrap Call-Form">
        <div class="Form-Cell">
            <form class="Form JS-Form"
                  data-form-email='ewgen.p@yandex.ua'
                  data-form-subject='Заказ с сайта Экотон'
                  data-form-url='/data.form.php'
                  data-form-method='POST'>

                <h2 class="Form-Title">Оставте ваш номер телефона и мы обязательно перезвоним!</h2>
                <svg class="Close-Form">
                    <use xlink:href="#close-2"></use>
                </svg>

                <input data-input-title='Заказчик звонка:' class="JS-Form-Require" type="text" placeholder="Ваше имя"
                       name="name">
                <input data-input-title='Позвоните заказчику по телефону:' class="JS-Form-Require" type="number"
                       placeholder="Телефон" name="phone">

                <button class="JS-Form-Button" type="submit">Отправить</button>
                <span class="JS-Form-Result"></span>
            </form>
        </div>
    </div>

    <div class="Form-Wrap Order-Form">
        <div class="Form-Cell">
            <form class="Order-Content Form JS-Form"
                  data-form-email='ewgen.p@yandex.ua'
                  data-form-url='/data.form.php'
                  data-form-method='POST'>

                <h2 class="Form-Title">Вы заказываете товар:</h2>
                <svg class="Close-Form">
                    <use xlink:href="#close-2"></use>
                </svg>

                <span class="Item-Title"></span>

                <input data-input-title='Заказ' class="Choosen-Item" type="text" value="" name="order">

                <input data-input-title='Ссылка на страницу заказа' class="Choosen-Link" type="text" value="" name="order-link">
                <input data-input-title='Имя заказчика' class="JS-Form-Require" type="text" placeholder="Ваше имя"
                       name="name">
                <input data-input-title='Телефон заказчика' class="JS-Form-Require" type="number" placeholder="Телефон"
                       name="phone">
                <textarea data-input-title="Сообщение от заказчика" placeholder="Сообщение" name="message"></textarea>
                <button class="JS-Form-Button" type="submit">Заказать</button>
                <span class="JS-Form-Result"></span>
            </form>
        </div>
    </div>

</footer><!-- .site-footer -->
<?php wp_enqueue_script("jquery"); ?>
<?php wp_footer(); ?>

<script src="/wp-includes/js/z-routine.min.js"></script>
</body>
</html>
