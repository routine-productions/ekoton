<?php
get_header();

?>
    <div id="site-content" class="site-content  Static-Page">
        <section class="static">
            <h2 class="Static-Title"><span><?= the_title() ?></span></h2>
            <?php
            the_post();
            echo the_content();?>
        </section>
    </div>
<?php


get_footer();
?>