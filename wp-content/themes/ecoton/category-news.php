<?php
/*
Template Name: Новости
*/
?>
<?php get_header(); ?>
<?php
//get_sidebar();
?>
<main id="site-content" class="News-Page-Wrap">
    <section class="News-Page">
    <h2><span>Новости</span></h2>
    <div class="News-Category">
        <?php
        $postsId = [];
        while(have_posts()){

                the_post();
                get_template_part('template-parts/post-news');
            $postsId[] = $post->ID;

        }
        ?>
        <div class="Paginate">
            <?php
            echo paginate_links();
            ?>
        </div>
    </div>
    </section>
    <div class="Latest-News-Wrap">
        <section class="Latest-News">

            <h2><span>Последние новости</span></h2>
            <?php
            $latest_news = get_posts([
                'category' => get_the_category()[0]->term_id,
                'posts_per_page' => 4,
                'exclude' => $postsId
            ]);

            foreach ($latest_news as $latest_one_news) {
                $post = $latest_one_news;
                get_template_part('template-parts/post-news-small');

            }
            ?>

        </section>
    </div>
</main>
<?php get_footer(); ?>
