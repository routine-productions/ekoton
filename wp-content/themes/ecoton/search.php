<?php
get_header(); ?>


<main id="site-content" class="site-content Search-Page" role="main">


    <?php
    if (have_posts()) {
        ?>
        <h2>
            <span><?php printf(__('Search Results for: %s', 'twentysixteen'), '<span>' . esc_html(get_search_query()) . '</span>'); ?></span>
        </h2>
        <div class="Search-Content">
            <?php
            while (have_posts()) {
            the_post();

            get_template_part('template-parts/post');
            }
            wp_reset_postdata();

            ?>
            <div class="Paginate">
                <?= paginate_links(); ?>
            </div>
        </div>
    <?php } else {
        get_template_part('template-parts/content-none');
    };
    ?>





</main>

<?php get_footer(); ?>
