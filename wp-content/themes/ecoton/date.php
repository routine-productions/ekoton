<?php
require_once __DIR__ . '/../../../wp-includes/class-wp-image-editor.php';
require_once __DIR__ . '/../../../wp-admin/includes/image.php';

$posts = get_posts(['post_type' => 'attachment', 'posts_per_page' => 100000]);
$i = 0;
foreach ($posts as $post)
{
    if ($i > 39)
    {
        $meta_value = wp_generate_attachment_metadata($post->ID, __DIR__ . '/../../../'. $post->guid);

//        print_r($meta_value);
        add_post_meta($post->ID, '_wp_attachment_metadata', '');
        wp_update_attachment_metadata($post->ID, $meta_value);
        if (! empty($post->guid))
        {
            $img = str_replace('/wp-content/uploads', '', $post->guid);
        }

        add_post_meta($post->ID, '_wp_attached_file', $img);
//        exit;
    }
    $i++;
}