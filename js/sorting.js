(function ($) {
    var sortMenu = $('.Conditions-Wrap'),
        sortCond = $('.Conditions'),
        sortButton = $('.Current-Condition');

    sortButton.live('click', function (event) {
        var listHeight = sortCond.outerHeight(true);
        var dataCurent = sortButton.find('li').attr("data-attr");
        var $item = sortCond.children('[data-href="'+dataCurent+'"]');
        $item.addClass('Active');
        $item.click(function(){
            if($(this).hasClass('Active')){
                return false;
            }
        });


        if (sortMenu.hasClass('Sort-Active')) {
            sortMenu.removeClass('Sort-Active').css({"height": 0});
            $(this).removeClass('Active');
        }
        else {
            sortMenu.addClass('Sort-Active').css({"height": listHeight + 'px'});
            $(this).addClass('Active');
        }

    });

    $('body').click(function (e) {

        if (!$(e.target).is('.Conditions a,.Conditions li ') && !$(e.target).is('.Js-Condition li, .Arrow svg ')) {

            sortMenu.removeClass('Sort-Active').css({"height": 0});
            sortButton.removeClass('Active');

        }
    });

    $(window).resize(function () {
        if ($(window).width() >= 1200) {
            sortMenu.removeClass('Sort-Active').css({"height": 0});
            sortButton.removeClass('Active');
        }
    });


})(jQuery);
