(function ($) {
    var hideManuf = function () {

        var hederHidden = $('.Site-Header-Bottom'),
            hederHiddenHeight = hederHidden.outerHeight(),
            $window = $(window),
            lastScrollTop = 0;
             var iterator = 0;

        var hideBottomMenu = function () {
            if(iterator >=3){
                iterator = 0;
            }else{
                iterator += 1;
            }
            var top =  $window.scrollTop();
            if(iterator == 3){
                if (lastScrollTop - top < 0) {

                    hederHidden.css({
                        transform: 'translateY(' + (-hederHiddenHeight) + 'px)'
                    });



                } else if(lastScrollTop - top > 0) {

                    hederHidden.css({
                        transform: 'translateY(0)'
                    });

                }

                lastScrollTop = top;
            }


        };
        $(window).scroll(function () {

                if( $window.width()>= 1200){
                    setTimeout(hideBottomMenu(), 0)
                }
            }
        );
    };
    hideManuf();

})(jQuery);