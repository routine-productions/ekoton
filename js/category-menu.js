(function ($) {
    var navMenu = $('.Js-Category'),
        navButton = $('.Js-Close-Cat');
    $('body').click(function (e) {

        if (!$(e.target).is('.Js-Category,.Nav-Active') && !$(e.target).is('.Js-Close-Cat svg,.Js-Close-Cat use') && !$(e.target).is('.Cat-Container, .cat-show')) {
            navMenu.removeClass('Nav-Active').css({"height": 0});
            navButton.removeClass('Btn-Active');
        }
    });


    navButton.live('click', function (event) {


        if (navMenu.hasClass('Nav-Active')) {
            navMenu.removeClass('Nav-Active').css({"height": 0});
            $(this).removeClass('Btn-Active');
        }
        else {
            navMenu.addClass('Nav-Active').css({"height": $('.Catalog-Aside').outerHeight() + 'px'});
            $(this).addClass('Btn-Active');
        }


        return false;
    });


    $(window).resize(function () {
        if ($(window).width() >= 1200) {
            navMenu.removeClass('Nav-Active').css({"height": 0});
            navButton.removeClass('Btn-Active');
        }
    });



})(jQuery);
