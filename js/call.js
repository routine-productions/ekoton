(function ($) {
    var callForm = $('.Call-Form'),
        callButton = $('.JS-Button');
    $('body').click(function (e) {
        if (!$(e.target).is('.Form, .Form h2, .Form input')) {
            callForm.removeClass('Form-Active');
            callButton.removeClass('Call-Btn-Active');
        }
    });


    callButton.live('click', function (event) {


        if (callForm.hasClass('Form-Active') ) {
            callForm.removeClass('Form-Active');
            $(this).removeClass('Call-Btn-Active');
        }
       else {
            callForm.addClass('Form-Active');
            $(this).addClass('Call-Btn-Active');
        }


        return false;
    });


})(jQuery);
