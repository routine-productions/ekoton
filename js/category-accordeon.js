(function ($) {
    var catButton = $('.cat-show');
    catButton.live('click', function (event) {
        var catCurrent = $(this).parent(),
            hiddenWrap = $(this).parent().find('> .Cat-Wrap'),
            catInnerHeight = $(this).parent().find('.Cat').height();

        if (catCurrent.hasClass('Cat-Active')) {
            catCurrent.removeClass('Cat-Active');
            $(this).removeClass('Btn-Active');
            hiddenWrap.css({"height": 0});
        }
        else {
            catCurrent.addClass('Cat-Active');
            $(this).addClass('Btn-Active');
            hiddenWrap.css({"height": catInnerHeight + 'px'});

        }
        return false;
    });
    $('.Catalog-Categories a').each(function () {
        if ($(this).attr('href')  + '/' == location.pathname) {
            $(this).addClass('Active-Category');
            $(this).parents('.Cat-In').find('>cat-show').trigger('click');
        }
    });
})(jQuery);

