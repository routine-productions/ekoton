(function ($) {
    var clickImg = $('.Js-Img'),
        views = $('.Views'),
        viewsList = $('.Views-List'),
        firstImg = $('.Views-List > .Js-Img:first'),
        viewItem = $('.View-Item');

    $(window).load(function () {
        firstImg.addClass('Active');
        if(views.width() < viewsList.width()){
            views.addClass('Scrolable');
        }else{
            views.removeClass('Scrolable');
        }
    });

    clickImg.live('click', function (e) {
        if(!$(e.target).is( '.Active')){
            var thisImg = $(this).attr("data-attr");
            // viewItem.css({'opacity' : '0'});
            setTimeout(function() {
                viewItem.css({'background-image' : 'url(' + thisImg + ')'});
            }, 200);
            $(this).addClass('Active').siblings().removeClass('Active');
            return false;
        }
    });
    $(window).resize(function () {
        if(views.width() < viewsList.width()){
            views.addClass('Scrolable');

        }else{
            views.removeClass('Scrolable');
        }
    });


})(jQuery);
