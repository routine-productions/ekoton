 (function ($) {
     var orderForm = $('.Order-Form'),
         orderButton = $('.Js-Order');
     $('body').click(function (e) {
         if (!$(e.target).is('.Form,.Form .Item-Title, .Form h2, .Form input, .Form textarea')) {
             orderForm.removeClass('Form-Active');
             orderButton.removeClass('Order-Btn-Active');
         }
     });

     orderButton.live('click', function (event) {
         if (orderForm.hasClass('Form-Active')) {
             orderForm.removeClass('Form-Active');
             $(this).removeClass('Order-Btn-Active');
         }
         else {
             orderForm.addClass('Form-Active');
             $(this).addClass('Order-Btn-Active');
             var orderText = $(this).closest('.Js-Post').find('.Js-Title').text();
             var orderLink = $(this).closest('.Js-Post').find('.Js-Link').attr("href");
             $('.Order-Form .Item-Title').html(orderText);
             $('.Order-Form .Choosen-Item').val(orderText);
             $('.Order-Form .Choosen-Link').val(orderLink);
         }
         return false;
     });


 })(jQuery);
