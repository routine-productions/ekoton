(function ($) {
    var searchField = $('.Search-Field');
    var searchBtn = $('.Search-Submit');
    searchBtn.click(function () {
        if (searchField.val().length < 1) {
            return false;
        }
    });
    searchBtn.on('hover',function () {
        if (searchField.val().length < 1) {
           $(this).css({'cursor':'default'});
        }else{
            $(this).css({'cursor':'pointer'});
        }
    });
    searchField.on('keydown',function () {
        if (searchField.val().length < 1) {
            searchBtn.css({'cursor':'default'});
        }else{
            searchBtn.css({'cursor':'pointer'});
        }
    });
})(jQuery);
