(function ($) {
    var navMenu = $('.Site-Nav'),
        navHeader = $('.Site-Header-Wrap'),
        navHeight = $('.Nav').outerHeight(),
        minHeight = $('.Site-Branding').outerHeight(true),
        navButton = $('.Ham-Wrap');

    navButton.live('click', function (event) {

        if (navHeader.hasClass('Naw-Head')) {
            navHeader.css({"height": minHeight + 'px'});
            setTimeout(function () {
                navHeader.removeClass('Naw-Head');
                navMenu.removeClass('Nav-Active Full Scroll');
            }, 200);

            $(this).removeClass('Btn-Active');
        }
        else if ($(window).height() >= (navHeight + minHeight)) {
            navMenu.addClass('Nav-Active Full').removeClass('Scroll');
            navHeader.addClass('Naw-Head').css({"height": $(window).outerHeight() + 'px'});
            $(this).addClass('Btn-Active');
        }
        else {
            navMenu.addClass('Nav-Active Scroll').removeClass('Full');
            navHeader.addClass('Naw-Head').css({"height": $(window).outerHeight() + 'px'});
            $(this).addClass('Btn-Active');
        }

        return false;
    });
    $(window).resize(function () {
        if ($(window).width() >= 1200 && navHeader.hasClass('Naw-Head')) {
            navMenu.removeClass('Nav-Active Full Scroll');
            navButton.removeClass('Btn-Active');
            navHeader.removeClass('Naw-Head').css({"height": '48px'});
        }

    });
    $(window).resize(function () {
        if ($(window).height() < (navHeight + minHeight) && navHeader.hasClass('Naw-Head')) {
            navMenu.removeClass('Full').addClass('Scroll');
            navHeader.css({"height": $(window).outerHeight() + 'px'});
        } else if ($(window).height() > (navHeight + minHeight) && navHeader.hasClass('Naw-Head')) {
            navMenu.removeClass('Scroll').addClass('Full');
            navHeader.css({"height": $(window).outerHeight() + 'px'});
        }
    });

})(jQuery);
